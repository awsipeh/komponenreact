import React from 'react';

const AppVar=() => {
    const nama = "Nursyfa Aprilianti";
    const absen = "26";

    return(
        <div>
            <h1 class="comp">Component Variabel</h1>
            <h2>Nama : {nama}</h2>
            <h3>Absen : {absen}</h3>
        </div>
    );
}

export default AppVar;